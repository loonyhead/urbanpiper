import React from 'react';
import Button from '../Button';

const InputDir = (props) => {
    const {value, name, displayName, handleChange, handleEnter} = props;
    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            handleEnter();
        }
      }
    return (
        <div>
            <input onChange={handleChange} onKeyDown={handleKeyDown} className="inputBox" rows="2" name={name} placeholder={displayName} value={value} />
        </div> 
    );
};


export default InputDir;
