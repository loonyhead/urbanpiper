import React from 'react';
import InputDir from '../InputDir';

const Tag = (props) => {
    const { title, onRemove } = props;
    return (
      <div>{title}<span onClick={onRemove}>X</span></div>
    );
  };
  
  
  export default Tag;