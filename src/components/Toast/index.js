import React from 'react';

const Toast = (props) => {
    const { data, close } = props;
    setTimeout(()=>close(), 2000);
        return (
            <div className={`toast ${data.type}`}>{data.msg}</div>
        );
  };
  
  
  export default Toast;