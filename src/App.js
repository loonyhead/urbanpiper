import React from 'react';
import { connect } from "react-redux";
import {
  onSubmit,
  onTagEnter,
  onTasteChange,
  onIntervalChange,
} from './actions';
import './App.css';
import InputDir from './components/InputDir';
import Tag from './components/Tag';
import Button from './components/Button';


const App = ({
  tags,
  currentTag,
  interval,
  onTagEnter,
  onSubmit,
  onTasteChange,
}) => {
  return (
    <div className="App">
        <InputDir handleChange={onTasteChange} handleEnter={onTagEnter} name="tag" displayName="Taste" value={currentTag} />
        <div>
          {
            tags.map(item=>(
                <Tag title={item} />
            ))
          }
        </div>
        <InputDir handleChange={onIntervalChange} name="interval" displayName="Iterval" value={interval} />
        <Button label="Submit" handleClick={onSubmit} />
    </div>
  );
};

const mapStateToProps = (state) => ({
    tags: state.tags,
    interval: state.interval,
    formatRules: state.formatRules,
    results: state.results,
    output: state.output,
    showingMsg: state.showingMsg,
    currentTag: state.currentTag,
});

export default connect(
  mapStateToProps, 
  {
    onSubmit,
    onTagEnter,
    onTasteChange,
    onIntervalChange,
  }
)(App);