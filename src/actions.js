import * as types from './constants';


export const onSubmit = () => {
    // ajax call
    // dispatch 
    return {
        type: types.SUBMIT_QUERY,
    }
};

export const onTasteChange = (e) => ({
    type: types.TASTE_TEXT_CHANGE,
    payload: e.target.value,
});

export const onTagEnter = () => ({
    type: types.CHANGE_TAG_ENTER,
});

export const onIntervalChange = (e) => ({
    type: types.ITERVAL_CHANGE,
    payload: e.target.value,
});