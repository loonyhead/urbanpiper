import { createStore } from 'redux';
import * as types from './constants';

const initialState = {
    tags: [],
    interval: 0,
    formatRules: {},
    results: [],
    output: null,
    showingMsg: false,
    currentTag: null,
};

const reducer = (state, {type, payload}) => {
    switch(type){
        case types.SUBMIT_QUERY: {
            // todo
        };

        case type.TASTE_TEXT_CHANGE: {
            return {
                ...state,
                currentTag: payload,
            };
        }

        case type.ITERVAL_CHANGE: {
            return {
                ...state,
                interval: payload,
            }
        }

        case type.CHANGE_TAG_ENTER: {
            return {
                ...state,
                tags: [...state.tags, state.currentTag],
            };
        }

        default:
            return state;
    }
};

export const store = createStore(
    reducer,
    initialState,
    window.devToolsExtension && window.devToolsExtension()
);
